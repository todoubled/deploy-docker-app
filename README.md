# Deploy a docker application to Ubuntu 16.04.4 LTS using an NGINX Reverse Proxy
This playbook will help you deploy your docker application or django apps to
ubuntu-16.04.4 LTS. The example below will setup a Django application service with a linked Postgres database.

In using the open source project [Trellis](https://github.com/roots/trellis) to
provision a site with HTTPS, you can then modify the NGINX configuration. The assumed first step is that you have provisioned a site on an Ubuntu 16.04.4 server using Trellis (v1.0.0-rc.2 as of this writing).

The instructions below guide you one-step further by deploying the docker dependencies,
then starting the service(s) using docker-compose.

## Install Instructions
**Step 1)** Install the `angstwad.docker.ubuntu` ansible role as defined within `requirements.yml` file. Place this updated file in your Trellis installation, then install it with:
```
ansible-galaxy install -r requirements.yml
```

**Step 2)** Move the playbook `deploy_app.yml` and the `group_vars/sites.yml` files into your Trellis path and modify the variables to fit your project.

**Step 3)** Run the playbook, the defaults added can be extracted into ansible `--extra-vars` if needed:
```
ansible-playbook deploy_app.yml
```

## NGINX configuration

**Step 4)** Update NGINX configuration to include the reverse proxy server block (/etc/nginx/sites-available/{domain}.conf)

Find the following server block:
```
  location / {
    try_files $uri $uri/ /index.php?$args;
  }
```

Replace it with the following and double-check the port used in the upstream:

```
upstream my_upstream {
  server 127.0.0.1:8000;
  keepalive 32;
}

location / {
  proxy_http_version 1.1;
  proxy_set_header Connection "";

  proxy_pass       http://my_upstream;
  proxy_set_header X-Forwarded-For $remote_addr;
  proxy_set_header X-Real-IP  $remote_addr;
  proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto https;
  proxy_set_header X-Forwarded-Port 443;
}

location /static/ {
  autoindex on;
  alias /srv/www/{{ domain }}/current/web/static/;
}

location ~ \.css {
  default_type text/css;
}

location ~ \.js {
  add_header  Content-Type    application/x-javascript;
}

```
** Please note ** There's a placeholder {{ domain }} above, add your domain and verify the `/srv/www/` path is correct for your installation.

The `location /static/` can be used for hosting your static files but can be optionally removed if you decide to go with a CDN instead.

**Step 5)** Final Step: Verify the NGINX configuration & reload

You will also want to first verify the configuration by doing a `service nginx configtest`. Then, you can reload NGINX by running `service nginx -s reload` to enable the new configuration.
